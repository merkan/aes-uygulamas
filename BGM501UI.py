# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'BGM501UI.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!
#
# Author Murat Erkan

import sys
from PyQt4 import QtCore
import resources_rc
import pyAES
import ctypes
from PyQt4.QtCore import *
from PyQt4.QtGui import *


myappid = u'mycompany.myproduct.subproduct.version' # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

try:
    _fromUtf8 = QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s
try:
    _encoding = QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        # type: (object, object, object) -> object
        return QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QApplication.translate(context, text, disambig)


class Ui_MainWindow(object):

    # Initialize GUI
    def setupUi(self, MainWindow):
        self.SBoxInput = []
        self.SBoxOutput = []
        MainWindow.setWindowModality(Qt.WindowModal)
        MainWindow.resize(776, 600)
        MainWindow.setMaximumSize(QSize(776, 800))
        MainWindow.setStyleSheet(_fromUtf8("border: none;background: white;margin:0;padding:0;"))
        MainWindow.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        MainWindow.setAnimated(True)
        MainWindow.setDockOptions(QMainWindow.AllowTabbedDocks | QMainWindow.AnimatedDocks)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setStyleSheet(_fromUtf8("padding:0px;margin:0px;"))
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setMargin(0)
        self.gridLayout.setSpacing(0)
        self.btnFrame = QFrame(self.centralwidget)
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnFrame.sizePolicy().hasHeightForWidth())
        self.btnFrame.setSizePolicy(sizePolicy)
        self.btnFrame.setObjectName(_fromUtf8("btnFrame"))
        self.btnFrame.setStyleSheet(_fromUtf8("#btnFrame {border: none;background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1,stop: 0.04 #a6a6a6, stop: 0.7 #7f7f7f,stop: 0 #717171, stop: 0.04 #626262,stop: 0.7 #4c4c4c, stop: 1 #333333);}"
                                              "#btnFrame QToolButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}\n"
                                              "#btnFrame QToolButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                              "#btnFrame QToolButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"))
        self.btnFrame.setFrameShape(QFrame.StyledPanel)
        self.btnFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout_2 = QGridLayout(self.btnFrame)
        self.gridLayout_2.setContentsMargins(10, 10, -1, 0)
        self.gridLayout_2.setSpacing(6)
        self.verticalLayout = QVBoxLayout()
        self.btnMainPage = QToolButton(self.btnFrame)
        self.btnMainPage.clicked.connect(self.showMainPage)
        self.verticalLayout.addWidget(self.btnMainPage)
        self.btnQ1_Menu = QToolButton(self.btnFrame)
        self.btnQ1_Menu.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ1_Menu.clicked.connect(self.showQ1)
        self.verticalLayout.addWidget(self.btnQ1_Menu)
        self.btnQ2_Menu = QToolButton(self.btnFrame)
        self.btnQ2_Menu.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ2_Menu.clicked.connect(self.showQ2)
        self.verticalLayout.addWidget(self.btnQ2_Menu)
        self.btnQ3_Menu = QToolButton(self.btnFrame)
        self.btnQ3_Menu.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ3_Menu.clicked.connect(self.showQ3)
        self.verticalLayout.addWidget(self.btnQ3_Menu)
        self.btnQ4_Menu = QToolButton(self.btnFrame)
        self.btnQ4_Menu.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ4_Menu.setStyleSheet(_fromUtf8(""))
        self.btnQ4_Menu.clicked.connect(self.showQ4)
        self.verticalLayout.addWidget(self.btnQ4_Menu)
        self.btnQ5_Menu = QToolButton(self.btnFrame)
        self.btnQ5_Menu.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ5_Menu.setStyleSheet(_fromUtf8(""))
        self.btnQ5_Menu.clicked.connect(self.showQ5)
        self.verticalLayout.addWidget(self.btnQ5_Menu)
        self.label = QLabel(self.btnFrame)
        self.label.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.label.setText(_fromUtf8(""))
        self.verticalLayout.addWidget(self.label)
        self.gridLayout_2.addLayout(self.verticalLayout, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.btnFrame, 1, 0, 1, 1)
        self.frame_2 = QFrame(self.centralwidget)
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.frame_2.setLineWidth(0)
        self.gridLayout_3 = QGridLayout(self.frame_2)
        self.gridLayout_3.setMargin(0)
        self.gridLayout_3.setSpacing(0)
        self.pages = QStackedWidget(self.frame_2)
        self.pages.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.pages.setStyleSheet(_fromUtf8("margin:0;padding:0;"))
        self.pages.setFrameShape(QFrame.StyledPanel)
        self.pages.setFrameShadow(QFrame.Sunken)
        self.pages.setLineWidth(0)
        self.page_1 = QWidget()
        self.gridLayout_4 = QGridLayout(self.page_1)
        self.mainPageLabel = QLabel(self.page_1)
        movie = QMovie(":/images/images/mainPage.gif")
        self.mainPageLabel.setMovie(movie)
        movie.start()
        self.mainPageLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignTop)
        self.gridLayout_4.addWidget(self.mainPageLabel, 0, 0, 1, 1)
        self.pages.addWidget(self.page_1)
        self.page_15 = QWidget()
        self.gridLayout_5 = QGridLayout(self.page_15)
        self.loadingPageLabel = QLabel(self.page_15)
        movie = QMovie(":/images/images/loading.gif")
        self.loadingPageLabel.setMovie(movie)
        movie.start()
        self.loadingPageLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.gridLayout_5.addWidget(self.loadingPageLabel, 0, 0, 1, 1)
        self.pages.addWidget(self.page_15)
        self.page_3 = QWidget()
        self.page_3.setObjectName(_fromUtf8("page_3"))
        self.page_3.setStyleSheet(_fromUtf8("#page_3 QSpinBox  {padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"))
        self.gridLayout_6 = QGridLayout(self.page_3)
        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setSpacing(6)
        self.label_2 = QLabel(self.page_3)
        self.label_2.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_2.setText(_fromUtf8(""))
        self.verticalLayout_3.addWidget(self.label_2)
        self.horizontalLayout_3 = QHBoxLayout()
        self.label_4 = QLabel(self.page_3)
        self.label_4.setMinimumSize(QtCore.QSize(50, 0))
        self.label_4.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_4.setStyleSheet(_fromUtf8("padding:3px"))
        self.horizontalLayout_3.addWidget(self.label_4)
        self.q1S1 = QSpinBox(self.page_3)
        self.q1S1.setMinimum(2)
        self.q1S1.setMaximum(99999)
        self.horizontalLayout_3.addWidget(self.q1S1)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QHBoxLayout()
        self.label_8 = QLabel(self.page_3)
        self.label_8.setMinimumSize(QtCore.QSize(50, 0))
        self.label_8.setMaximumSize(QtCore.QSize(50, 16777215))
        self.label_8.setStyleSheet(_fromUtf8("padding:3px"))
        self.horizontalLayout_4.addWidget(self.label_8)
        self.q1S2 = QSpinBox(self.page_3)
        self.q1S2.setMinimum(2)
        self.q1S2.setMaximum(99999)
        self.horizontalLayout_4.addWidget(self.q1S2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QHBoxLayout()
        self.q1ErrTxt = QLabel(self.page_3)
        font = QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.q1ErrTxt.setFont(font)
        self.q1ErrTxt.setStyleSheet(_fromUtf8("padding:10px;padding-left:50px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_5.addWidget(self.q1ErrTxt)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        self.label_3 = QLabel(self.page_3)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_3.sizePolicy().hasHeightForWidth())
        self.label_3.setSizePolicy(sizePolicy)
        self.label_3.setText(_fromUtf8(""))
        self.verticalLayout_3.addWidget(self.label_3)
        self.btnCont_1 = QFrame(self.page_3)
        self.btnCont_1.setObjectName(_fromUtf8("btnCont_1"))
        self.btnCont_1.setStyleSheet(_fromUtf8("#btnCont_1 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_1 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_1 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_1 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_1 QLabel {background:transparent;border:none;}"))
        self.btnCont_1.setFrameShape(QFrame.StyledPanel)
        self.btnCont_1.setFrameShadow(QFrame.Raised)
        self.gridLayout_7 = QGridLayout(self.btnCont_1)
        self.gridLayout_7.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_6 = QHBoxLayout()
        self.label_7 = QLabel(self.btnCont_1)
        self.label_7.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.label_7.setText(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.label_7)
        self.btnQ1_1 = QPushButton(self.btnCont_1)
        self.btnQ1_1.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ1_1.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ1_1.clicked.connect(self.calculateQ1)
        self.horizontalLayout_6.addWidget(self.btnQ1_1)
        self.label_6 = QLabel(self.btnCont_1)
        self.label_6.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.horizontalLayout_6.addWidget(self.label_6)
        self.gridLayout_7.addLayout(self.horizontalLayout_6, 0, 0, 1, 1)
        self.verticalLayout_3.addWidget(self.btnCont_1)
        self.gridLayout_6.addLayout(self.verticalLayout_3, 0, 0, 1, 1)
        self.pages.addWidget(self.page_3)
        self.page_4 = QWidget()
        self.gridLayout_8 = QGridLayout(self.page_4)
        self.verticalLayout_4 = QVBoxLayout()
        self.label_10 = QLabel(self.page_4)
        self.label_10.setMinimumSize(QtCore.QSize(0, 30))
        self.label_10.setMaximumSize(QtCore.QSize(16777215, 30))
        self.verticalLayout_4.addWidget(self.label_10)
        self.horizontalLayout_7 = QHBoxLayout()
        self.label_13 = QLabel(self.page_4)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_13.sizePolicy().hasHeightForWidth())
        self.label_13.setSizePolicy(sizePolicy)
        self.label_13.setFont(font)
        self.label_13.setStyleSheet(_fromUtf8("padding:3px;"))
        self.horizontalLayout_7.addWidget(self.label_13)
        self.q1ResultTxt = QLabel(self.page_4)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.q1ResultTxt.sizePolicy().hasHeightForWidth())
        self.q1ResultTxt.setSizePolicy(sizePolicy)
        self.q1ResultTxt.setFont(font)
        self.q1ResultTxt.setStyleSheet(_fromUtf8("padding: 10px;"))
        self.horizontalLayout_7.addWidget(self.q1ResultTxt)
        self.verticalLayout_4.addLayout(self.horizontalLayout_7)
        self.q1Table = QTableWidget(self.page_4)
        self.q1Table.setColumnCount(0)
        self.q1Table.setRowCount(0)
        self.q1Table.setStyleSheet(_fromUtf8("border:1px solid #333333"))
        self.q1Table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.verticalLayout_4.addWidget(self.q1Table)
        self.btnCont_2 = QFrame(self.page_4)
        self.btnCont_2.setObjectName(_fromUtf8("btnCont_2"))
        self.btnCont_2.setStyleSheet(_fromUtf8("#btnCont_2 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_2 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_2 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_2 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_2 QLabel {background:transparent;border:none;}"))

        self.btnCont_2.setFrameShape(QFrame.StyledPanel)
        self.btnCont_2.setFrameShadow(QFrame.Raised)
        self.gridLayout_9 = QGridLayout(self.btnCont_2)
        self.gridLayout_9.setContentsMargins(0, 9, 0, 9)
        self.gridLayout_9.setSpacing(6)
        self.horizontalLayout_8 = QHBoxLayout()
        self.label_15 = QLabel(self.btnCont_2)
        self.label_15.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.horizontalLayout_8.addWidget(self.label_15)
        self.btnQ1_2 = QPushButton(self.btnCont_2)
        self.btnQ1_2.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ1_2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ1_2.clicked.connect(self.showQ1)
        self.horizontalLayout_8.addWidget(self.btnQ1_2)
        self.label_14 = QLabel(self.btnCont_2)
        self.label_14.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.horizontalLayout_8.addWidget(self.label_14)
        self.gridLayout_9.addLayout(self.horizontalLayout_8, 0, 0, 1, 1)
        self.verticalLayout_4.addWidget(self.btnCont_2)
        self.gridLayout_8.addLayout(self.verticalLayout_4, 0, 0, 1, 1)
        self.pages.addWidget(self.page_4)
        self.page_5 = QWidget()
        self.page_5.setObjectName(_fromUtf8("page_5"))
        self.page_5.setStyleSheet(_fromUtf8("#page_5 QSpinBox{padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"))
        self.gridLayout_10 = QGridLayout(self.page_5)
        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setSizeConstraint(QLayout.SetMaximumSize)
        self.verticalLayout_5.setSpacing(6)
        self.label_16 = QLabel(self.page_5)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_16.sizePolicy().hasHeightForWidth())
        self.label_16.setSizePolicy(sizePolicy)
        self.label_16.setMinimumSize(QtCore.QSize(0, 30))
        self.label_16.setMaximumSize(QtCore.QSize(16777215, 30))
        self.verticalLayout_5.addWidget(self.label_16)
        self.horizontalLayout_10 = QHBoxLayout()
        self.label_20 = QLabel(self.page_5)
        self.label_20.setMinimumSize(QtCore.QSize(90, 0))
        self.label_20.setMaximumSize(QtCore.QSize(90, 16777215))
        self.label_20.setStyleSheet(_fromUtf8("padding:3px;"))
        self.horizontalLayout_10.addWidget(self.label_20)
        self.q2S1 = QSpinBox(self.page_5)
        self.q2S1.setMinimum(1)
        self.q2S1.setMaximum(1000)
        self.horizontalLayout_10.addWidget(self.q2S1)
        self.verticalLayout_5.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11 = QHBoxLayout()
        self.label_21 = QLabel(self.page_5)
        self.label_21.setMinimumSize(QtCore.QSize(90, 0))
        self.label_21.setMaximumSize(QtCore.QSize(90, 16777215))
        self.label_21.setStyleSheet(_fromUtf8("padding:3px;"))
        self.horizontalLayout_11.addWidget(self.label_21)
        self.q2S2 = QSpinBox(self.page_5)
        self.q2S2.setMinimum(2)
        self.q2S2.setMaximum(100)
        self.horizontalLayout_11.addWidget(self.q2S2)
        self.verticalLayout_5.addLayout(self.horizontalLayout_11)
        self.horizontalLayout_13 = QHBoxLayout()
        self.label_23 = QLabel(self.page_5)
        self.label_23.setMinimumSize(QtCore.QSize(90, 0))
        self.label_23.setMaximumSize(QtCore.QSize(90, 16777215))
        self.label_23.setStyleSheet(_fromUtf8("padding:3px;"))
        self.horizontalLayout_13.addWidget(self.label_23)
        self.q2S3 = QSpinBox(self.page_5)
        self.q2S3.setMinimum(2)
        self.q2S3.setMaximum(100)
        self.horizontalLayout_13.addWidget(self.q2S3)
        self.verticalLayout_5.addLayout(self.horizontalLayout_13)
        self.horizontalLayout_12 = QHBoxLayout()
        self.q2ErrTxt = QLabel(self.page_5)
        self.q2ErrTxt.setFont(font)
        self.q2ErrTxt.setStyleSheet(_fromUtf8("padding:10px;padding-left:90px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_12.addWidget(self.q2ErrTxt)
        self.verticalLayout_5.addLayout(self.horizontalLayout_12)
        self.label_17 = QLabel(self.page_5)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_17.sizePolicy().hasHeightForWidth())
        self.label_17.setSizePolicy(sizePolicy)
        self.label_17.setText(_fromUtf8(""))
        self.verticalLayout_5.addWidget(self.label_17)
        self.btnCont_3 = QFrame(self.page_5)
        self.btnCont_3.setObjectName(_fromUtf8("btnCont_3"))
        self.btnCont_3.setStyleSheet(_fromUtf8("#btnCont_3 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_3 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_3 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_3 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_3 QLabel {background:transparent;border:none;}"))

        self.btnCont_3.setFrameShape(QFrame.StyledPanel)
        self.btnCont_3.setFrameShadow(QFrame.Raised)
        self.gridLayout_11 = QGridLayout(self.btnCont_3)
        self.gridLayout_11.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_9 = QHBoxLayout()
        self.label_19 = QLabel(self.btnCont_3)
        self.label_19.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.label_19.setText(_fromUtf8(""))
        self.horizontalLayout_9.addWidget(self.label_19)
        self.btnQ2_1 = QPushButton(self.btnCont_3)
        self.btnQ2_1.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ2_1.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ2_1.clicked.connect(self.calculateQ2)
        self.horizontalLayout_9.addWidget(self.btnQ2_1)
        self.label_18 = QLabel(self.btnCont_3)
        self.label_18.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.horizontalLayout_9.addWidget(self.label_18)
        self.gridLayout_11.addLayout(self.horizontalLayout_9, 0, 0, 1, 1)
        self.verticalLayout_5.addWidget(self.btnCont_3)
        self.gridLayout_10.addLayout(self.verticalLayout_5, 0, 0, 1, 1)
        self.pages.addWidget(self.page_5)
        self.page_6 = QWidget()
        self.gridLayout_12 = QGridLayout(self.page_6)
        self.verticalLayout_6 = QVBoxLayout()
        self.q2Table = QTableWidget(self.page_6)
        self.q2Table.setColumnCount(0)
        self.q2Table.setRowCount(0)
        self.q2Table.setStyleSheet(_fromUtf8("border:1px solid #333333"))
        self.verticalLayout_6.addWidget(self.q2Table)
        self.q2Table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.btnCont_4 = QFrame(self.page_6)
        self.btnCont_4.setObjectName(_fromUtf8("btnCont_4"))
        self.btnCont_4.setStyleSheet(_fromUtf8("#btnCont_4 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_4 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_4 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_4 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_4 QLabel {background:transparent;border:none;}"))
        self.btnCont_4.setFrameShape(QFrame.StyledPanel)
        self.btnCont_4.setFrameShadow(QFrame.Raised)
        self.gridLayout_13 = QGridLayout(self.btnCont_4)
        self.gridLayout_13.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_14 = QHBoxLayout()
        self.label_27 = QLabel(self.btnCont_4)
        self.label_27.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.label_27.setText(_fromUtf8(""))
        self.horizontalLayout_14.addWidget(self.label_27)
        self.btnQ2_2 = QPushButton(self.btnCont_4)
        self.btnQ2_2.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ2_2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ2_2.clicked.connect(self.showQ2)
        self.horizontalLayout_14.addWidget(self.btnQ2_2)
        self.label_26 = QLabel(self.btnCont_4)
        self.label_26.setStyleSheet(_fromUtf8("border: none;background: transparent;"))
        self.horizontalLayout_14.addWidget(self.label_26)
        self.gridLayout_13.addLayout(self.horizontalLayout_14, 0, 0, 1, 1)
        self.verticalLayout_6.addWidget(self.btnCont_4)
        self.gridLayout_12.addLayout(self.verticalLayout_6, 0, 0, 1, 1)
        self.pages.addWidget(self.page_6)
        self.page_7 = QWidget()
        self.gridLayout_14 = QGridLayout(self.page_7)
        self.verticalLayout_7 = QVBoxLayout()
        self.inputCont = QFrame(self.page_7)
        self.inputCont.setObjectName(_fromUtf8("inputCont"))
        self.inputCont.setStyleSheet(_fromUtf8("#inputCont QLabel {padding:3px;width:60px;}"
                                               "#inputCont QLineEdit  {padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"
                                               "#inputCont QComboBox  {padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"
                                               "#inputCont QPlainTextEdit  {padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"))
        self.inputCont.setFrameShape(QFrame.StyledPanel)
        self.inputCont.setFrameShadow(QFrame.Raised)
        self.gridLayout_16 = QGridLayout(self.inputCont)
        self.gridLayout_16.setMargin(0)
        self.verticalLayout_8 = QVBoxLayout()
        self.horizontalLayout_16 = QHBoxLayout()
        self.label_25 = QLabel(self.inputCont)
        self.label_25.setMinimumSize(QtCore.QSize(100, 0))
        self.label_25.setMaximumSize(QtCore.QSize(100, 90))
        self.horizontalLayout_16.addWidget(self.label_25)
        self.q3Combo_1 = QComboBox(self.inputCont)
        self.q3Combo_1.setMaximumSize(QtCore.QSize(1000, 16777215))
        self.horizontalLayout_16.addWidget(self.q3Combo_1)
        self.verticalLayout_8.addLayout(self.horizontalLayout_16)
        self.horizontalLayout_26 = QHBoxLayout()
        self.horizontalLayout_26.setContentsMargins(-1, 0, -1, -1)
        self.label_43 = QLabel(self.inputCont)
        self.label_43.setMinimumSize(QtCore.QSize(100, 0))
        self.label_43.setMaximumSize(QtCore.QSize(100, 16777215))
        self.horizontalLayout_26.addWidget(self.label_43)
        self.q3Combo_2 = QComboBox(self.inputCont)
        self.horizontalLayout_26.addWidget(self.q3Combo_2)
        self.verticalLayout_8.addLayout(self.horizontalLayout_26)
        self.horizontalLayout_17 = QHBoxLayout()
        self.label_32 = QLabel(self.inputCont)
        self.label_32.setMinimumSize(QtCore.QSize(100, 0))
        self.label_32.setMaximumSize(QtCore.QSize(100, 16777215))
        self.horizontalLayout_17.addWidget(self.label_32)
        self.q3Combo_3 = QComboBox(self.inputCont)
        self.horizontalLayout_17.addWidget(self.q3Combo_3)
        self.verticalLayout_8.addLayout(self.horizontalLayout_17)
        self.horizontalLayout_18 = QHBoxLayout()
        self.label_30 = QLabel(self.inputCont)
        self.label_30.setMinimumSize(QtCore.QSize(100, 0))
        self.label_30.setMaximumSize(QtCore.QSize(100, 16777215))
        self.horizontalLayout_18.addWidget(self.label_30)
        self.q3Edit_1 = QLineEdit(self.inputCont)
        self.horizontalLayout_18.addWidget(self.q3Edit_1)
        self.verticalLayout_8.addLayout(self.horizontalLayout_18)
        self.bvCont = QFrame(self.inputCont)
        self.bvCont.setFrameShape(QFrame.StyledPanel)
        self.bvCont.setFrameShadow(QFrame.Raised)
        self.gridLayout_17 = QGridLayout(self.bvCont)
        self.gridLayout_17.setMargin(0)
        self.gridLayout_17.setSpacing(0)
        self.horizontalLayout_19 = QHBoxLayout()
        self.horizontalLayout_19.setContentsMargins(0, 0, -1, -1)
        self.label_31 = QLabel(self.bvCont)
        self.label_31.setMinimumSize(QtCore.QSize(107, 0))
        self.label_31.setMaximumSize(QtCore.QSize(107, 100))
        self.label_31.setVisible(False)
        self.horizontalLayout_19.addWidget(self.label_31)
        self.q3Edit_2 = QLineEdit(self.bvCont)
        self.q3Edit_2.setVisible(False)
        self.horizontalLayout_19.addWidget(self.q3Edit_2)
        self.gridLayout_17.addLayout(self.horizontalLayout_19, 0, 0, 1, 1)
        self.verticalLayout_8.addWidget(self.bvCont)
        self.horizontalLayout_20 = QHBoxLayout()
        self.label_33 = QLabel(self.inputCont)
        self.label_33.setMinimumSize(QtCore.QSize(100, 0))
        self.label_33.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_33.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.horizontalLayout_20.addWidget(self.label_33)
        self.q3PlainEdit = QPlainTextEdit(self.inputCont)
        self.horizontalLayout_20.addWidget(self.q3PlainEdit)
        self.verticalLayout_8.addLayout(self.horizontalLayout_20)
        self.horizontalLayout_22 = QHBoxLayout()
        self.horizontalLayout_22.setContentsMargins(-1, 0, -1, -1)
        self.q3ErrTxt = QLabel(self.inputCont)
        self.q3ErrTxt.setFont(font)
        self.q3ErrTxt.setStyleSheet(_fromUtf8("padding:10px;padding-left:100px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_22.addWidget(self.q3ErrTxt)
        self.verticalLayout_8.addLayout(self.horizontalLayout_22)
        self.gridLayout_16.addLayout(self.verticalLayout_8, 0, 0, 1, 1)
        self.verticalLayout_7.addWidget(self.inputCont)
        self.btnCont_5 = QFrame(self.page_7)
        self.btnCont_5.setObjectName(_fromUtf8("btnCont_5"))
        self.btnCont_5.setStyleSheet(_fromUtf8("#btnCont_5 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_5 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_5 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_5 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_5 QLabel {background:transparent;border:none;}"))
        self.btnCont_5.setFrameShape(QFrame.StyledPanel)
        self.btnCont_5.setFrameShadow(QFrame.Raised)
        self.gridLayout_15 = QGridLayout(self.btnCont_5)
        self.gridLayout_15.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_15 = QHBoxLayout()
        self.label_29 = QLabel(self.btnCont_5)
        self.horizontalLayout_15.addWidget(self.label_29)
        self.btnQ3_1 = QPushButton(self.btnCont_5)
        self.btnQ3_1.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ3_1.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ3_1.clicked.connect(self.calculateQ3)
        self.horizontalLayout_15.addWidget(self.btnQ3_1)
        self.label_28 = QLabel(self.btnCont_5)
        self.label_28.setText(_fromUtf8(""))
        self.horizontalLayout_15.addWidget(self.label_28)
        self.gridLayout_15.addLayout(self.horizontalLayout_15, 0, 0, 1, 1)
        self.verticalLayout_7.addWidget(self.btnCont_5)
        self.gridLayout_14.addLayout(self.verticalLayout_7, 0, 0, 1, 1)
        self.pages.addWidget(self.page_7)
        self.page_8 = QWidget()
        self.page_8.setObjectName(_fromUtf8("page_8"))
        self.page_8.setStyleSheet(_fromUtf8("#page_8 QLabel {padding:3px;}"))
        self.gridLayout_18 = QGridLayout(self.page_8)
        self.verticalLayout_9 = QVBoxLayout()
        self.label_24 = QLabel(self.page_8)
        self.label_24.setMinimumSize(QtCore.QSize(0, 30))
        self.label_24.setMaximumSize(QtCore.QSize(16777215, 30))
        self.verticalLayout_9.addWidget(self.label_24)
        self.horizontalLayout_23 = QHBoxLayout()
        self.label_37 = QLabel(self.page_8)
        self.label_37.setMinimumSize(QtCore.QSize(100, 0))
        self.label_37.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_37.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.horizontalLayout_23.addWidget(self.label_37)
        self.q3Result_1 = QTextEdit(self.page_8)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.q3Result_1.sizePolicy().hasHeightForWidth())
        self.q3Result_1.setSizePolicy(sizePolicy)
        self.q3Result_1.setMaximumSize(QtCore.QSize(16777215, 50))
        self.q3Result_1.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard | QtCore.Qt.TextSelectableByMouse)
        self.horizontalLayout_23.addWidget(self.q3Result_1)
        self.verticalLayout_9.addLayout(self.horizontalLayout_23)
        self.horizontalLayout_24 = QHBoxLayout()
        self.label_39 = QLabel(self.page_8)
        self.label_39.setMinimumSize(QtCore.QSize(100, 0))
        self.label_39.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_39.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.horizontalLayout_24.addWidget(self.label_39)
        self.q3Result_2 = QTextEdit(self.page_8)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.q3Result_2.sizePolicy().hasHeightForWidth())
        self.q3Result_2.setSizePolicy(sizePolicy)
        self.q3Result_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.q3Result_2.setOverwriteMode(True)
        self.q3Result_2.setTextInteractionFlags(QtCore.Qt.TextSelectableByKeyboard | QtCore.Qt.TextSelectableByMouse)
        self.horizontalLayout_24.addWidget(self.q3Result_2)
        self.verticalLayout_9.addLayout(self.horizontalLayout_24)
        self.horizontalLayout_25 = QHBoxLayout()
        self.label_40 = QLabel(self.page_8)
        self.label_40.setMinimumSize(QtCore.QSize(100, 0))
        self.label_40.setMaximumSize(QtCore.QSize(100, 16777215))
        self.label_40.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.horizontalLayout_25.addWidget(self.label_40)
        self.q3TxtBrw = QTextBrowser(self.page_8)
        self.q3TxtBrw.setFont(font)
        self.q3TxtBrw.viewport().setProperty("cursor", QCursor(QtCore.Qt.IBeamCursor))
        self.horizontalLayout_25.addWidget(self.q3TxtBrw)
        self.verticalLayout_9.addLayout(self.horizontalLayout_25)
        self.btnCont_6 = QFrame(self.page_8)
        self.btnCont_6.setObjectName(_fromUtf8("btnCont_6"))
        self.btnCont_6.setStyleSheet(_fromUtf8("#btnCont_6 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_6 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_6 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_6 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_6 QLabel {background:transparent;border:none;}"))
        self.btnCont_6.setFrameShape(QFrame.StyledPanel)
        self.btnCont_6.setFrameShadow(QFrame.Raised)
        self.gridLayout_19 = QGridLayout(self.btnCont_6)
        self.gridLayout_19.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_21 = QHBoxLayout()
        self.label_41 = QLabel(self.btnCont_6)
        self.horizontalLayout_21.addWidget(self.label_41)
        self.btnQ3_2 = QPushButton(self.btnCont_6)
        self.btnQ3_2.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ3_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.btnQ3_2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.btnQ3_2.clicked.connect(self.showQ3)
        self.horizontalLayout_21.addWidget(self.btnQ3_2)
        self.label_42 = QLabel(self.btnCont_6)
        self.horizontalLayout_21.addWidget(self.label_42)
        self.gridLayout_19.addLayout(self.horizontalLayout_21, 0, 0, 1, 1)
        self.verticalLayout_9.addWidget(self.btnCont_6)
        self.gridLayout_18.addLayout(self.verticalLayout_9, 0, 0, 1, 1)
        self.pages.addWidget(self.page_8)
        self.page_9 = QWidget()
        self.page_9.setObjectName(_fromUtf8("page_9"))
        self.page_9.setStyleSheet(_fromUtf8("#page_9 QSpinBox  {padding: 5px;transition: border 0.3s;border:2px solid #c9c9c9;border-radius:5px;}"
                                            "#page_9 QLabel {padding:3px;}"))
        self.gridLayout_20 = QGridLayout(self.page_9)
        self.verticalLayout_10 = QVBoxLayout()
        self.label_34 = QLabel(self.page_9)
        self.label_34.setMinimumSize(QtCore.QSize(0, 30))
        self.label_34.setMaximumSize(QtCore.QSize(16777215, 30))
        self.verticalLayout_10.addWidget(self.label_34)
        self.horizontalLayout_35 = QHBoxLayout()
        self.horizontalLayout_35.setContentsMargins(-1, 0, -1, -1)
        self.q4Title1 = QLabel(self.page_9)
        self.q4Title1.setFont(font)
        self.q4Title1.setStyleSheet(_fromUtf8("padding:10px;padding-left:0px;"))
        self.horizontalLayout_35.addWidget(self.q4Title1)
        self.verticalLayout_10.addLayout(self.horizontalLayout_35)
        self.horizontalLayout_28 = QHBoxLayout()
        self.label_47 = QLabel(self.page_9)
        self.label_47.setMinimumSize(QtCore.QSize(100, 0))
        self.label_47.setMaximumSize(QtCore.QSize(100, 16777215))
        self.horizontalLayout_28.addWidget(self.label_47)
        self.q4S1 = QSpinBox(self.page_9)
        self.q4S1.setMinimum(2)
        self.q4S1.setMaximum(100)
        self.horizontalLayout_28.addWidget(self.q4S1)
        self.verticalLayout_10.addLayout(self.horizontalLayout_28)
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, 0, -1, -1)
        self.label_5 = QLabel(self.page_9)
        self.label_5.setMinimumSize(QtCore.QSize(100, 0))
        self.label_5.setMaximumSize(QtCore.QSize(100, 16777215))
        self.horizontalLayout.addWidget(self.label_5)
        self.q4S2 = QSpinBox(self.page_9)
        self.q4S2.setMinimum(2)
        self.q4S2.setMaximum(100)
        self.horizontalLayout.addWidget(self.q4S2)
        self.verticalLayout_10.addLayout(self.horizontalLayout)
        self.horizontalLayout_29 = QHBoxLayout()
        self.q4ErrTxt = QLabel(self.page_9)
        self.q4ErrTxt.setFont(font)
        self.q4ErrTxt.setStyleSheet(_fromUtf8("padding:10px;padding-left:100px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_29.addWidget(self.q4ErrTxt)
        self.verticalLayout_10.addLayout(self.horizontalLayout_29)
        self.label_44 = QLabel(self.page_9)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_44.sizePolicy().hasHeightForWidth())
        self.label_44.setSizePolicy(sizePolicy)
        self.label_44.setText(_fromUtf8(""))
        self.verticalLayout_10.addWidget(self.label_44)
        self.btnCont_7 = QFrame(self.page_9)
        self.btnCont_7.setObjectName(_fromUtf8("btnCont_7"))
        self.btnCont_7.setStyleSheet(_fromUtf8("#btnCont_7 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_7 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_7 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_7 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_7 QLabel {background:transparent;border:none;}"))
        self.btnCont_7.setFrameShape(QFrame.StyledPanel)
        self.btnCont_7.setFrameShadow(QFrame.Raised)
        self.gridLayout_21 = QGridLayout(self.btnCont_7)
        self.horizontalLayout_27 = QHBoxLayout()
        self.label_46 = QLabel(self.btnCont_7)
        self.label_46.setText(_fromUtf8(""))
        self.horizontalLayout_27.addWidget(self.label_46)
        self.btnQ4_1 = QPushButton(self.btnCont_7)
        self.btnQ4_1.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ4_1.setMaximumSize(QtCore.QSize(16777215, 50))
        self.btnQ4_1.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.horizontalLayout_27.addWidget(self.btnQ4_1)
        self.label_45 = QLabel(self.btnCont_7)
        self.horizontalLayout_27.addWidget(self.label_45)
        self.gridLayout_21.addLayout(self.horizontalLayout_27, 0, 0, 1, 1)
        self.verticalLayout_10.addWidget(self.btnCont_7)
        self.gridLayout_20.addLayout(self.verticalLayout_10, 0, 0, 1, 1)
        self.pages.addWidget(self.page_9)
        self.page_10 = QWidget()
        self.gridLayout_22 = QGridLayout(self.page_10)
        self.verticalLayout_11 = QVBoxLayout()
        self.label_49 = QLabel(self.page_10)
        self.label_49.setMinimumSize(QtCore.QSize(0, 30))
        self.label_49.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_49.setText(_fromUtf8(""))
        self.verticalLayout_11.addWidget(self.label_49)
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(-1, 0, -1, -1)
        self.q4Title2 = QLabel(self.page_10)
        self.q4Title2.setFont(font)
        self.q4Title2.setStyleSheet(_fromUtf8("padding:10px;"))
        self.q4Title2.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.horizontalLayout_3.addWidget(self.q4Title2)
        self.verticalLayout_11.addLayout(self.horizontalLayout_3)
        self.q4Table = QTableWidget(self.page_10)
        self.q4Table.setColumnCount(0)
        self.q4Table.setRowCount(0)
        self.q4Table.setStyleSheet(_fromUtf8("border:1px solid #333333"))
        self.verticalLayout_11.addWidget(self.q4Table)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(-1, 0, -1, -1)
        self.q4ErrTxt_2 = QLabel(self.page_10)
        self.q4ErrTxt_2.setFont(font)
        self.q4ErrTxt_2.setStyleSheet(_fromUtf8("padding:10px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_2.addWidget(self.q4ErrTxt_2)
        self.verticalLayout_11.addLayout(self.horizontalLayout_2)
        self.btnCont_8 = QFrame(self.page_10)
        self.btnCont_8.setObjectName(_fromUtf8("btnCont_8"))
        self.btnCont_8.setStyleSheet(_fromUtf8("#btnCont_8 {border: none;background:transparent;border-radius: 3px;}"
                                                "#btnCont_8 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                                "#btnCont_8 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                                "#btnCont_8 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                                "#btnCont_8 QLabel {background:transparent;border:none;}"))
        self.btnCont_8.setFrameShape(QFrame.StyledPanel)
        self.btnCont_8.setFrameShadow(QFrame.Raised)
        self.gridLayout_23 = QGridLayout(self.btnCont_8)
        self.gridLayout_23.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_30 = QHBoxLayout()
        self.label_52 = QLabel(self.btnCont_8)
        self.label_52.setText(_fromUtf8(""))
        self.horizontalLayout_30.addWidget(self.label_52)
        self.btnQ4_2 = QPushButton(self.btnCont_8)
        self.btnQ4_2.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ4_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.btnQ4_2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.horizontalLayout_30.addWidget(self.btnQ4_2)
        self.label_51 = QLabel(self.btnCont_8)
        self.horizontalLayout_30.addWidget(self.label_51)
        self.gridLayout_23.addLayout(self.horizontalLayout_30, 0, 0, 1, 1)
        self.verticalLayout_11.addWidget(self.btnCont_8)
        self.gridLayout_22.addLayout(self.verticalLayout_11, 0, 0, 1, 1)
        self.pages.addWidget(self.page_10)
        self.page_11 = QWidget()
        self.gridLayout_24 = QGridLayout(self.page_11)
        self.verticalLayout_12 = QVBoxLayout()
        self.label_50 = QLabel(self.page_11)
        self.label_50.setMinimumSize(QtCore.QSize(0, 30))
        self.label_50.setMaximumSize(QtCore.QSize(16777215, 30))
        self.verticalLayout_12.addWidget(self.label_50)
        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setContentsMargins(-1, 0, -1, -1)
        self.q4Title3 = QLabel(self.page_11)
        self.q4Title3.setFont(font)
        self.q4Title3.setStyleSheet(_fromUtf8("padding:10px;"))
        self.q4Title3.setAlignment(QtCore.Qt.AlignLeading | QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.horizontalLayout_4.addWidget(self.q4Title3)
        self.verticalLayout_12.addLayout(self.horizontalLayout_4)
        self.q5Table = QTableWidget(self.page_11)
        self.q5Table.setColumnCount(0)
        self.q5Table.setRowCount(0)
        self.q5Table.setStyleSheet(_fromUtf8("border:1px solid #333333"))
        self.verticalLayout_12.addWidget(self.q5Table)
        self.horizontalLayout_33 = QHBoxLayout()
        self.horizontalLayout_33.setContentsMargins(-1, 0, -1, -1)
        self.q5ErrTxt = QLabel(self.page_11)
        self.q5ErrTxt.setFont(font)
        self.q5ErrTxt.setStyleSheet(_fromUtf8("padding:10px;color:rgb(109, 39, 39)"))
        self.horizontalLayout_33.addWidget(self.q5ErrTxt)
        self.verticalLayout_12.addLayout(self.horizontalLayout_33)
        self.btnCont_9 = QFrame(self.page_11)
        self.btnCont_9.setObjectName(_fromUtf8("btnCont_9"))
        self.btnCont_9.setStyleSheet(_fromUtf8("#btnCont_9 {border: none;background:transparent;border-radius: 3px;}"
                                                "#btnCont_9 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                                "#btnCont_9 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                                "#btnCont_9 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                                "#btnCont_9 QLabel {background:transparent;border:none;}"))
        self.btnCont_9.setFrameShape(QFrame.StyledPanel)
        self.btnCont_9.setFrameShadow(QFrame.Raised)
        self.gridLayout_25 = QGridLayout(self.btnCont_9)
        self.horizontalLayout_31 = QHBoxLayout()
        self.label_55 = QLabel(self.btnCont_9)
        self.label_55.setText(_fromUtf8(""))
        self.horizontalLayout_31.addWidget(self.label_55)
        self.btnQ5_2 = QPushButton(self.btnCont_9)
        self.btnQ5_2.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ5_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.btnQ5_2.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.horizontalLayout_31.addWidget(self.btnQ5_2)
        self.label_54 = QLabel(self.btnCont_9)
        self.horizontalLayout_31.addWidget(self.label_54)
        self.gridLayout_25.addLayout(self.horizontalLayout_31, 0, 0, 1, 1)
        self.verticalLayout_12.addWidget(self.btnCont_9)
        self.gridLayout_24.addLayout(self.verticalLayout_12, 0, 0, 1, 1)
        self.pages.addWidget(self.page_11)
        self.page_12 = QWidget()
        self.gridLayout_26 = QGridLayout(self.page_12)
        self.verticalLayout_13 = QVBoxLayout()
        self.label_56 = QLabel(self.page_12)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(30)
        sizePolicy.setHeightForWidth(self.label_56.sizePolicy().hasHeightForWidth())
        self.label_56.setSizePolicy(sizePolicy)
        self.label_56.setMinimumSize(QtCore.QSize(0, 30))
        self.label_56.setMaximumSize(QtCore.QSize(16777215, 30))
        self.label_56.setText(_fromUtf8(""))
        self.verticalLayout_13.addWidget(self.label_56)
        self.q4ResultTable = QTableWidget(self.page_12)
        self.q4ResultTable.setColumnCount(0)
        self.q4ResultTable.setRowCount(0)
        self.q4ResultTable.setStyleSheet(_fromUtf8("border:1px solid #333333"))
        self.verticalLayout_13.addWidget(self.q4ResultTable)
        self.btnCont_10 = QFrame(self.page_12)
        self.btnCont_10.setObjectName(_fromUtf8("btnCont_10"))
        self.btnCont_10.setStyleSheet(_fromUtf8("#btnCont_10 {border: none;background:transparent;border-radius: 3px;}"
                                               "#btnCont_10 QPushButton {color: #333;border: 2px solid #555;border-radius: 11px;padding: 5px;background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #888);min-width: 80px;}"
                                               "#btnCont_10 QPushButton:hover {background: qradialgradient(cx: 0.3, cy: -0.4,fx: 0.3, fy: -0.4,radius: 1.35, stop: 0 #fff, stop: 1 #bbb);}"
                                               "#btnCont_10 QPushButton:pressed {background: qradialgradient(cx: 0.4, cy: -0.1,fx: 0.4, fy: -0.1,radius: 1.35, stop: 0 #fff, stop: 1 #ddd);}"
                                               "#btnCont_10 QLabel {background:transparent;border:none;}"))
        self.btnCont_10.setFrameShape(QFrame.StyledPanel)
        self.btnCont_10.setFrameShadow(QFrame.Raised)
        self.gridLayout_27 = QGridLayout(self.btnCont_10)
        self.horizontalLayout_32 = QHBoxLayout()
        self.label_58 = QLabel(self.btnCont_10)
        self.horizontalLayout_32.addWidget(self.label_58)
        self.btnQ4_3 = QPushButton(self.btnCont_10)
        self.btnQ4_3.setMinimumSize(QtCore.QSize(94, 50))
        self.btnQ4_3.setMaximumSize(QtCore.QSize(16777215, 50))
        self.btnQ4_3.setCursor(QCursor(QtCore.Qt.PointingHandCursor))
        self.horizontalLayout_32.addWidget(self.btnQ4_3)
        self.label_57 = QLabel(self.btnCont_10)
        self.horizontalLayout_32.addWidget(self.label_57)
        self.gridLayout_27.addLayout(self.horizontalLayout_32, 0, 0, 1, 1)
        self.verticalLayout_13.addWidget(self.btnCont_10)
        self.gridLayout_26.addLayout(self.verticalLayout_13, 0, 0, 1, 1)
        self.pages.addWidget(self.page_12)
        self.gridLayout_3.addWidget(self.pages, 0, 0, 1, 1)
        self.gridLayout.addWidget(self.frame_2, 1, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.retranslateUi(MainWindow)
        self.initialize()
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "BGM 501 Implementasyon", None))
        self.btnMainPage.setText(_translate("MainWindow", "Ana Sayfa", None))
        self.btnQ1_Menu.setText(_translate("MainWindow", "Soru 1", None))
        self.btnQ2_Menu.setText(_translate("MainWindow", "Soru 2", None))
        self.btnQ3_Menu.setText(_translate("MainWindow", "Soru 3", None))
        self.btnQ4_Menu.setText(_translate("MainWindow", "Soru4", None))
        self.btnQ5_Menu.setText(_translate("MainWindow", "Soru5", None))
        self.label_4.setText(_translate("MainWindow", "Sayı 1 :", None))
        self.label_8.setText(_translate("MainWindow", "Sayı 2 :", None))
        self.q1ErrTxt.setText(_translate("MainWindow", "", None))
        self.btnQ1_1.setText(_translate("MainWindow", "Hesapla", None))
        self.label_13.setText(_translate("MainWindow", "EBOB", None))
        self.btnQ1_2.setText(_translate("MainWindow", "Geri Dön", None))
        self.label_20.setText(_translate("MainWindow", "Başlangıç Sayısı ", None))
        self.label_21.setText(_translate("MainWindow", "Bitiş Sayısı", None))
        self.label_23.setText(_translate("MainWindow", "Mod", None))
        self.q2ErrTxt.setText(_translate("MainWindow", "", None))
        self.btnQ2_1.setText(_translate("MainWindow", "Hesapla", None))
        self.btnQ2_2.setText(_translate("MainWindow", "Geri Dön", None))
        self.label_25.setText(_translate("MainWindow", "Mode", None))
        self.label_43.setText(_translate("MainWindow", "AES Tipi", None))
        self.label_32.setText(_translate("MainWindow", "Tipi", None))
        self.label_30.setText(_translate("MainWindow", "Şifre", None))
        self.label_31.setText(_translate("MainWindow", "Başlangıç Vektörü", None))
        self.label_33.setText(_translate("MainWindow", "Metin", None))
        self.q3ErrTxt.setText(_translate("MainWindow", "", None))
        self.btnQ3_1.setText(_translate("MainWindow", "Şifrele ve Şifre Çöz", None))
        self.label_37.setText(_translate("MainWindow", "Şifre", None))
        self.q3Result_1.setText(_translate("MainWindow", "", None))
        self.label_39.setText(_translate("MainWindow", "Başlangıç Vektörü", None))
        self.q3Result_2.setText(_translate("MainWindow", "", None))
        self.label_40.setText(_translate("MainWindow", "Metin", None))
        self.q3TxtBrw.setHtml(_translate("MainWindow",
                                         "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
                                         "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
                                         "p, li { white-space: pre-wrap; }\n"
                                         "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:600; font-style:normal;\">\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">gadgadfgdfgadfg</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">gadfg</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">adf</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">g</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">adf</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">g</span></p>\n"
                                         "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">adfgdfag</span></p></body></html>",
                                         None))
        self.btnQ3_2.setText(_translate("MainWindow", "Geri Dön", None))
        self.label_47.setText(_translate("MainWindow", "S Box (Sutun)", None))
        self.label_5.setText(_translate("MainWindow", "S Box (Satır)", None))
        self.q4ErrTxt.setText(_translate("MainWindow", "", None))
        self.btnQ4_1.setText(_translate("MainWindow", "Tablo Oluştur", None))
        self.q4ErrTxt_2.setText(_translate("MainWindow", "", None))
        self.btnQ4_2.setText(_translate("MainWindow", "Farksal Tablo Hesapla", None))
        self.q5ErrTxt.setText(_translate("MainWindow", "", None))
        self.btnQ5_2.setText(_translate("MainWindow", "Doğrusal Yaklaşım Tablosu Hesapla", None))
        self.btnQ4_3.setText(_translate("MainWindow", "Geri Dön", None))
        self.q4Title2.setText(_translate("MainWindow", "S Kutusu Girdi Değerleri", None))
        self.q4Title3.setText(_translate("MainWindow", "S Kutusu Çıktı Değerleri", None))

    def initialize(self):
        self.q3Combo_1.addItem(unicode("Şifrele", 'utf-8'))
        self.q3Combo_1.addItem(unicode("Şifre Çöz", 'utf-8'))
        self.q3Combo_2.addItem(unicode("256 Bit", 'utf-8'))
        self.q3Combo_2.addItem(unicode("128 Bit", 'utf-8'))
        self.q3Combo_3.addItem(unicode("EKK - Elektronik Kod Kitabı", 'utf-8'))
        self.q3Combo_3.addItem(unicode("ŞBZ - Sifre Blok Zinciri", 'utf-8'))
        self.q3Combo_3.addItem(unicode("ÇGB - Çıktı Geri Besleme", 'utf-8'))
        self.q3Combo_3.currentIndexChanged.connect(self.changeType)
        self.pages.setCurrentIndex(0)

    def changeType(self):
        val1 = self.q3Combo_3.currentIndex()
        if val1 == 0:
            self.q3Edit_2.setVisible(False)
            self.label_31.setVisible(False)
        else:
            self.q3Edit_2.setVisible(True)
            self.label_31.setVisible(True)


    def showMainPage(self):
        self.pages.setCurrentIndex(0)

    def showQ1(self):
        self.q1S1.setValue(1)
        self.q1S2.setValue(1)
        self.q1ErrTxt.setText(_translate("MainWindow", "", None))
        self.pages.setCurrentIndex(2)

    def showQ2(self):
        self.q2S1.setValue(1)
        self.q2S2.setValue(2)
        self.q2S3.setValue(2)
        self.q2ErrTxt.setText(_translate("MainWindow", "", None))
        self.pages.setCurrentIndex(4)

    def showQ3(self):
        self.q3Combo_1.setCurrentIndex(0)
        self.q3Combo_2.setCurrentIndex(0)
        self.q3Combo_3.setCurrentIndex(0)
        self.q3Edit_2.setVisible(False)
        self.label_31.setVisible(False)
        self.q3Edit_1.setText(_translate("MainWindow", "", None))
        self.q3Edit_2.setText(_translate("MainWindow", "", None))
        self.q3PlainEdit.setPlainText(_translate("MainWindow", "", None))
        self.q3ErrTxt.setText(_translate("MainWindow", "", None))
        self.pages.setCurrentIndex(6)

    def showQ4(self):
        self.q4S1.setValue(2)
        self.q4S2.setValue(2)
        self.q4ErrTxt.setText(_translate("MainWindow", "", None))
        self.q5ErrTxt.setText(_translate("MainWindow", "", None))
        self.q4Title1.setText(_translate("MainWindow", "Fark Tablosu", None))
        self.btnQ4_1.clicked.connect(self.showSBoxInputDifferential)
        self.pages.setCurrentIndex(8)


    def showSBoxInputDifferential(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q4Table.setRowCount(rowNumber)
        self.q4Table.setColumnCount(colmNumber)
        headers = []
        for x in xrange(colmNumber):
            headers.append(_translate("MainWindow", str(x), None))
            for y in xrange(rowNumber):
                self.q4Table.setItem(y, x,QTableWidgetItem("0"))
        self.q4Table.setHorizontalHeaderLabels(headers)
        self.q4Table.resizeColumnsToContents()
        self.q4Table.resizeRowsToContents()
        self.pages.setCurrentIndex(9)
        self.btnQ4_2.setText(_translate("MainWindow", "S Kutusu Çıktı Değerlerini Oluştur", None))
        self.btnQ4_2.clicked.connect(self.showSBoxOutputDifferential)

    def showSBoxOutputDifferential(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q4ErrTxt_2.setText(_translate("MainWindow", "", None))
        self.SBoxInput = []
        try:
            for x in xrange(colmNumber):
                for y in xrange(rowNumber):
                    item = int(self.q4Table.item(y,x).text())
                    if item < 0:
                        self.q4ErrTxt_2.setText(_translate("MainWindow", "Girmiş olduğunuz değer 0 küçük olamaz ("+str(x)+","+str(y)+")", None))
                        break
                    if item > 15:
                        self.q4ErrTxt_2.setText(_translate("MainWindow", "Girmiş olduğunuz değer 0xf büyük olamaz ("+str(x)+","+str(y)+")", None))
                        break
                    self.SBoxInput.append(item)
        except:
            self.q4ErrTxt_2.setText(_translate("MainWindow", "Girmiş olduğunuz değerler sayısal olmalıdır",None))
            pass
        if len(self.q4ErrTxt_2.text()) > 5:
            self.pages.setCurrentIndex(9)
            self.btnQ4_2.setText(_translate("MainWindow", "S Kutusu Çıktı Değerlerini Oluştur", None))
        else:
            self.q5Table.setRowCount(rowNumber)
            self.q5Table.setColumnCount(colmNumber)
            headers = []
            for x in xrange(colmNumber):
                headers.append(_translate("MainWindow", str(x), None))
                for y in xrange(rowNumber):
                    self.q5Table.setItem(x, y, QTableWidgetItem("0"))
            self.q5Table.setHorizontalHeaderLabels(headers)
            self.q5Table.resizeColumnsToContents()
            self.q5Table.resizeRowsToContents()
            self.btnQ5_2.clicked.connect(self.calculateQ4)
            self.btnQ5_2.setText(_translate("MainWindow", "Fark Tablosunu Göster",None))
            self.q4ErrTxt.setText(_translate("MainWindow", "", None))
            self.pages.setCurrentIndex(10)

    def showQ5(self):
        self.q4S1.setValue(2)
        self.q4S2.setValue(2)
        self.q4ErrTxt.setText(_translate("MainWindow", "", None))
        self.q5ErrTxt.setText(_translate("MainWindow", "", None))
        self.q4Title1.setText(_translate("MainWindow", "Doğrusal Yaklaşım Tablosu", None))
        self.pages.setCurrentIndex(8)
        self.btnQ4_1.clicked.connect(self.showSBoxInputLAT)

    def showSBoxInputLAT(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q4Table.setRowCount(rowNumber)
        self.q4Table.setColumnCount(colmNumber)
        headers = []
        for x in xrange(colmNumber):
            headers.append(_translate("MainWindow", str(x), None))
            for y in xrange(rowNumber):
                self.q4Table.setItem(y, x, QTableWidgetItem("0"))
        self.q4Table.setHorizontalHeaderLabels(headers)
        self.q4Table.resizeColumnsToContents()
        self.q4Table.resizeRowsToContents()
        self.pages.setCurrentIndex(9)
        self.btnQ4_2.setText(_translate("MainWindow", "S Kutusu Çıktı Değerlerini Oluştur", None))
        self.btnQ4_2.clicked.connect(self.showSBoxOutputLAT)

    def showSBoxOutputLAT(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q4ErrTxt_2.setText(_translate("MainWindow", "", None))
        self.SBoxInput = []
        try:
            for x in xrange(colmNumber):
                for y in xrange(rowNumber):
                    item = int(self.q4Table.item(y, x).text())
                    if item < 0:
                        self.q4ErrTxt_2.setText(_translate("MainWindow",
                                                           "Girmiş olduğunuz değer 0 küçük olamaz (" + str(
                                                               x) + "," + str(y) + ")", None))
                        break
                    if item > 15:
                        self.q4ErrTxt_2.setText(_translate("MainWindow",
                                                           "Girmiş olduğunuz değer 0xf büyük olamaz (" + str(
                                                               x) + "," + str(y) + ")", None))
                        break
                    self.SBoxInput.append(item)
        except:
            self.q4ErrTxt_2.setText(_translate("MainWindow", "Girmiş olduğunuz değerler sayısal olmalıdır", None))
            pass
        if len(self.q4ErrTxt_2.text()) > 5:
            self.pages.setCurrentIndex(9)
            self.btnQ4_2.setText(_translate("MainWindow", "S Kutusu Çıktı Değerlerini Oluştur", None))
        else:
            self.q5Table.setRowCount(rowNumber)
            self.q5Table.setColumnCount(colmNumber)
            headers = []
            for x in xrange(colmNumber):
                headers.append(_translate("MainWindow",str(x), None))
                for y in xrange(rowNumber):
                    self.q5Table.setItem(x, y, QTableWidgetItem("0"))
            self.q5Table.setHorizontalHeaderLabels(headers)
            self.q5Table.resizeColumnsToContents()
            self.q5Table.resizeRowsToContents()
            self.btnQ5_2.clicked.connect(self.calculateQ5)
            self.btnQ5_2.setText(_translate("MainWindow", "Doğrusal Yaklaşım Tablosunu Göster", None))
            self.q4ErrTxt.setText(_translate("MainWindow", "", None))
            self.pages.setCurrentIndex(10)

    def calculateQ1(self):
        self.thread = CalculateThread(self, 1)
        self.thread.finished.connect(self.calculateQ1Result)
        self.thread.start()
        self.pages.setCurrentIndex(1)

    def calculateQ1Result(self, obj):
        self.q1ResultTxt.setText(str(obj[0]))
        headers = []
        if len(obj[2]) != 1:
            self.q1Table.setRowCount(len(obj[2]))
            self.q1Table.setColumnCount(4)
            headers.append(_translate("MainWindow", "Büyük Sayı", None))
            headers.append(_translate("MainWindow", "Küçük Sayı", None))
            headers.append(_translate("MainWindow", "Bölüm", None))
            headers.append(_translate("MainWindow", "Kalan", None))
            for number in xrange(len(obj[2])):
                self.q1Table.setItem(number, 0, QTableWidgetItem(str(obj[2][number])))
                self.q1Table.setItem(number, 1, QTableWidgetItem(str(obj[3][number])))
                self.q1Table.setItem(number, 2, QTableWidgetItem(str(obj[1][number])))
                self.q1Table.setItem(number, 3, QTableWidgetItem(str(obj[2][number] % obj[3][number])))

        else:
            self.q1Table.clear()
            self.q1Table.clearContents()
            self.q1Table.clearSpans()

        self.q1Table.setHorizontalHeaderLabels(headers)
        self.q1Table.resizeColumnsToContents()
        self.q1Table.resizeRowsToContents()
        self.pages.setCurrentIndex(3)
        if self.thread.isRunning():
            self.thread.terminate()

    def calculateQ2(self):
        self.thread = CalculateThread(self, 2)
        self.thread.finished.connect(self.calculateQ2Result)
        self.thread.start()
        self.pages.setCurrentIndex(1)

    def calculateQ2Result(self, obj):
        if len(obj) != 1:
            self.q2Table.setRowCount(len(obj))
            self.q2Table.setColumnCount(3)
            headers = []
            headers.append(_translate("MainWindow", "Sayı", None))
            headers.append(_translate("MainWindow", "Sayının Tersi", None))
            headers.append(_translate("MainWindow", "Mod", None))
            for number in xrange(len(obj)):
                self.q2Table.setItem(number, 0, QTableWidgetItem(str(obj[number][0])))
                self.q2Table.setItem(number, 1, QTableWidgetItem(str(obj[number][1])))
                self.q2Table.setItem(number, 2, QTableWidgetItem(str(obj[number][2])))
            self.q1Table.horizontalHeader().setDefaultAlignment(QtCore.Qt.AlignHCenter)
            self.q2Table.setHorizontalHeaderLabels(headers)
            self.q2Table.resizeColumnsToContents()
            self.q2Table.resizeRowsToContents()
        self.pages.setCurrentIndex(5)
        if self.thread.isRunning():
            self.thread.terminate()

    def calculateQ3(self):
        passw = self.q3Edit_1.text()
        if len(passw) < 1:
            self.q3ErrTxt.setText(_translate("MainWindow", "Şifre Giriniz", None))
            return
        typeIndex = self.q3Combo_3.currentIndex()
        bv = self.q3Edit_2.text()
        if len(bv) < 1 and typeIndex == 1:
            self.q3ErrTxt.setText(_translate("MainWindow", "Başlangıç Vektörü Giriniz", None))
            return
        text = self.q3PlainEdit.toPlainText()
        if len(text) < 1:
            self.q3ErrTxt.setText(_translate("MainWindow", "Metin Giriniz", None))
            return

        self.thread = CalculateThread(self, 3)
        self.thread.finished.connect(self.calculateQ3Result)
        self.thread.start()
        self.pages.setCurrentIndex(1)

    def calculateQ3Result(self, obj):
        if len(obj) > 0:
            self.q3Result_1.setText(_translate("MainWindow", obj[0], None))
            if len(obj[1]) > 0:
                self.q3Result_2.setText(_translate("MainWindow", obj[1], None))
                self.q3Result_2.setVisible(True)
                self.label_39.setVisible(True)
            else:
                self.q3Result_2.setVisible(False)
                self.label_39.setVisible(False)
            self.q3TxtBrw.setPlainText(_translate("MainWindow", obj[2], None))
        self.pages.setCurrentIndex(7)
        if self.thread.isRunning():
            self.thread.terminate()

    def calculateQ4(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q5ErrTxt.setText(_translate("MainWindow","", None))
        self.SBoxOutput = []
        try:
            for x in xrange(colmNumber):
                for y in xrange(rowNumber):
                    item = int(self.q5Table.item(x, y).text())
                    if item < 0:
                        self.q5ErrTxt.setText(_translate("MainWindow","Girmiş olduğunuz değer 0 küçük olamaz (" + str(x) + "," + str(y) + ")", None))
                        break
                    if item > 15:
                        self.q5ErrTxt.setText(_translate("MainWindow","Girmiş olduğunuz değer 0xf büyük olamaz (" + str(x) + "," + str(y) + ")", None))
                        break
                    self.SBoxOutput.append(item)
        except:
            self.q5ErrTxt.setText(_translate("MainWindow", "Girmiş olduğunuz değerler sayısal olmalıdır", None))
        if len(self.q5ErrTxt.text()) > 5:
            self.pages.setCurrentIndex(10)
            self.btnQ5_2.clicked.connect(self.calculateQ4)
            self.btnQ5_2.setText(_translate("MainWindow", "Fark Tablosunu Göster", None))
        else:
            self.thread = CalculateThread(self, 4)
            self.thread.finished.connect(self.calculateQ4Result)
            self.thread.start()
            self.pages.setCurrentIndex(1)

    def calculateQ4Result(self,obj):
        if len(obj) != 1:
            colmNumber = len(obj)
            rowNumber = len(obj[0])
            self.q4ResultTable.setRowCount(rowNumber)
            self.q4ResultTable.setColumnCount(colmNumber)
            headers = []
            for x in xrange(colmNumber):
                headers.append(_translate("MainWindow",  str(x), None))
                for y in xrange(rowNumber):
                    self.q4ResultTable.setItem(y, x, QTableWidgetItem(str(obj[x][y])))
            self.q4ResultTable.setHorizontalHeaderLabels(headers)
            self.q4ResultTable.resizeColumnsToContents()
            self.q4ResultTable.resizeRowsToContents()
        self.btnQ4_3.clicked.connect(self.showQ4)
        self.pages.setCurrentIndex(11)
        if self.thread.isRunning():
            self.thread.terminate()

    def calculateQ5(self):
        colmNumber = int(self.q4S1.value())
        rowNumber = int(self.q4S2.value())
        self.q5ErrTxt.setText(_translate("MainWindow", "", None))
        self.SBoxOutput = []
        try:
            for x in xrange(colmNumber):
                for y in xrange(rowNumber):
                    item = int(self.q5Table.item(x, y).text())
                    if item < 0:
                        self.q5ErrTxt.setText(_translate("MainWindow","Girmiş olduğunuz değer 0 küçük olamaz (" + str(x) + "," + str(y) + ")", None))
                        break
                    if item > 15:
                        self.q5ErrTxt.setText(_translate("MainWindow","Girmiş olduğunuz değer 0xf büyük olamaz (" + str(x) + "," + str(y) + ")", None))
                        break
                    self.SBoxOutput.append(item)
        except:
            self.q5ErrTxt.setText(_translate("MainWindow", "Girmiş olduğunuz değerler sayısal olmalıdır", None))
        if len(self.q5ErrTxt.text()) > 5:
            self.pages.setCurrentIndex(10)
            self.btnQ5_2.clicked.connect(self.calculateQ4)
            self.btnQ5_2.setText(_translate("MainWindow", "Doğrusal Yaklaşım Tablosunu Göster", None))
        else:
            self.thread = CalculateThread(self, 5)
            self.thread.finished.connect(self.calculateQ5Result)
            self.thread.start()
            self.pages.setCurrentIndex(1)

    def calculateQ5Result(self,obj):
        if len(obj) != 1:
            colmNumber = len(obj)
            rowNumber = len(obj[0])
            self.q4ResultTable.setRowCount(rowNumber)
            self.q4ResultTable.setColumnCount(colmNumber)
            headers = []
            for x in xrange(colmNumber):
                headers.append(_translate("MainWindow", str(x) , None))
                for y in xrange(rowNumber):
                    self.q4ResultTable.setItem(y, x, QTableWidgetItem(str(obj[x][y])))
            self.q4ResultTable.setHorizontalHeaderLabels(headers)
            self.q4ResultTable.resizeColumnsToContents()
            self.q4ResultTable.resizeRowsToContents()
        self.btnQ4_3.clicked.connect(self.showQ5)
        self.pages.setCurrentIndex(11)
        if self.thread.isRunning():
            self.thread.terminate()

class CalculateThread(QThread):
    finished = QtCore.pyqtSignal(object)

    def __init__(self, MainWindow, Type):
        QThread.__init__(self)
        self.mainWindow = MainWindow
        self.type = Type
        self.mutex = QMutex()

    def __del__(self):
        self.wait()

    def convert_qst_to_str(self,text):
        return ''.join(map(unichr, [text.at(i).unicode() for i in range(len(text))]))

    def process(self):
        if self.type == 1:
            num1 = self.mainWindow.q1S1.value()
            num2 = self.mainWindow.q1S2.value()
            ebob, division, maxNumbers, minNumbers = pyAES.Euclid(num1, num2)
            self.finished.emit((ebob, division, maxNumbers, minNumbers))
        elif self.type == 2:
            table = []
            startNumber = self.mainWindow.q2S1.value()
            finishNumber = self.mainWindow.q2S2.value()
            mod = self.mainWindow.q2S3.value()
            while startNumber < finishNumber:
                ebob, division, maxNumbers, minNumbers = pyAES.Euclid(startNumber, mod)
                if ebob == 1 and startNumber != 1:
                    inverse = pyAES.CalculateInverseByCoprimeNumber(division)
                    table.append([startNumber, inverse, mod])
                else:
                    table.append([startNumber, " - ", mod])
                startNumber = startNumber + 1
            self.finished.emit(table)
        elif self.type == 3:
            var1 = self.mainWindow.q3Combo_1.currentIndex()
            var2 = self.mainWindow.q3Combo_2.currentIndex()
            var3 = self.mainWindow.q3Combo_3.currentIndex()
            tmp = self.mainWindow.q3Edit_1.text()
            passw =  self.convert_qst_to_str(tmp).encode("utf-8")
            tmp = self.mainWindow.q3Edit_2.text()
            bv = self.convert_qst_to_str(tmp).encode("utf-8")
            tmp = self.mainWindow.q3PlainEdit.toPlainText()
            text = self.convert_qst_to_str(tmp).encode("utf-8")
            if var1 == 0 and var2 == 0 and var3 == 0:
                text2 = pyAES.EKKModeEncrypt(passw,text)
            elif var1 == 0 and var2 == 0 and var3 == 1:
                text2 = pyAES.SBZModeEncrypt(passw,bv,text)
            elif var1 == 0 and var2 == 0 and var3 == 2:
                text2 = pyAES.CGBModeEncrypt(passw,bv,text)
            elif var1 == 0 and var2 == 1 and var3 == 0:
                text2 = pyAES.EKKModeEncrypt(passw,text,pyAES.TYPES.B128)
            elif var1 == 0 and var2 == 1 and var3 == 1:
                text2 = pyAES.SBZModeEncrypt(passw,bv,text,pyAES.TYPES.B128)
            elif var1 == 0 and var2 == 1 and var3 == 2:
                text2 = pyAES.CGBModeEncrypt(passw,bv,text,pyAES.TYPES.B128)
            elif var1 == 1 and var2 == 0 and var3 == 0:
                text2 = pyAES.EKKModeDecrypt(passw,text)
            elif var1 == 1 and var2 == 0 and var3 == 1:
                text2 = pyAES.SBZModeDecrypt(passw,bv,text)
            elif var1 == 1 and var2 == 0 and var3 == 2:
                text2 = pyAES.CGBModeDecrypt(passw,bv,text)
            elif var1 == 1 and var2 == 1 and var3 == 0:
                text2 = pyAES.EKKModeDecrypt(passw,text,pyAES.TYPES.B128)
            elif var1 == 1 and var2 == 1 and var3 == 1:
                text2 = pyAES.SBZModeDecrypt(passw,bv,text,pyAES.TYPES.B128)
            elif var1 == 1 and var2 == 1 and var3 == 2:
                text2 = pyAES.CGBModeDecrypt(passw,bv,text,pyAES.TYPES.B128)
            self.finished.emit((passw, bv, text2))
        elif self.type == 4:
            si = self.mainWindow.SBoxInput
            so = self.mainWindow.SBoxOutput
            colmNumber = int(self.mainWindow.q4S1.value())
            rowNumber = int(self.mainWindow.q4S2.value())
            table = pyAES.DifferentialTable(si,so,rowNumber,colmNumber)
            self.finished.emit(table)
        elif self.type == 5:
            si = self.mainWindow.SBoxInput
            so = self.mainWindow.SBoxOutput
            colmNumber = int(self.mainWindow.q4S1.value())
            rowNumber = int(self.mainWindow.q4S2.value())
            table = pyAES.LatTable(si,so,rowNumber,colmNumber)
            self.finished.emit(table)
        else:
            self.finished.emit(None)

    def run(self):
        try:
            self.mutex.lock()
            self.process()
            self.mutex.unlock()
        except :
            print "hata"
            print  sys.exc_info()[0]
            pass
        return


if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(Dialog)
    app.setWindowIcon(QIcon(_fromUtf8(":/images/images/logo.png")))
    Dialog.show()
    sys.exit(app.exec_())
