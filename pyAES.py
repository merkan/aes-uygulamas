# -*- coding: utf-8 -*-


import hashlib
from copy import copy
import base64

sbox = [
        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16
        ]

sboxInv = [
        0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
        0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
        0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
        0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
        0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
        0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
        0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
        0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
        0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
        0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
        0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
        0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
        0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
        0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
        0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
        0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d
        ]

rcon = [
        0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
        0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
        0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
        0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
        0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
        0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
        0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
        0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
        0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
        0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
        0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
        0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
        0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
        0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
        0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
        0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb
        ]

def enum(**enums):
    return type('Enum', (), enums)

TYPES = enum(B128=10,B256=14)

TEXTORBYTE = enum(TEXT=0,BYTE=1)

def rotate(word, n):
    return word[n:]+word[0:n]

def shiftRows(state):
    for i in range(4):
        state[i*4:i*4+4] = rotate(state[i*4:i*4+4],i)


def shiftRowsInv(state):
    for i in range(4):
        state[i*4:i*4+4] = rotate(state[i*4:i*4+4],-i)

def keyScheduleCore(word, i):
    word = rotate(word, 1)
    newWord = []
    for byte in word:
        index = int(byte)
        newWord.append(sbox[index])
    newWord[0] = newWord[0]^rcon[i]
    return newWord


def expandKey(cipherKey,mode = TYPES.B256):
    cipherKeySize = len(cipherKey)
    if mode == TYPES.B256:
        assert cipherKeySize == 32
    else:
        assert cipherKeySize == 16

    expandedKey = []
    currentSize = 0
    rconIter = 1
    t = [0,0,0,0]
    for i in range(cipherKeySize):
        expandedKey.append(cipherKey[i])
    currentSize += cipherKeySize
    while currentSize < 240:
        for i in range(4):
            t[i] = expandedKey[(currentSize - 4) + i]
        if currentSize % cipherKeySize == 0:
            t = keyScheduleCore(t, rconIter)
            rconIter += 1
        if currentSize % cipherKeySize == 16:
            for i in range(4):
                t[i] = sbox[t[i]]
        for i in range(4):
            expandedKey.append(((expandedKey[currentSize - cipherKeySize]) ^ (t[i])))
            currentSize += 1
            
    return expandedKey


def subBytes(state):
    for i in range(len(state)):
        state[i] = sbox[state[i]]

# inverse sbox transform on each byte in state table
def subBytesInv(state):
    for i in range(len(state)):
        state[i] = sboxInv[state[i]]


def addRoundKey(state, roundKey):
    for i in range(len(state)):
        state[i] = state[i] ^ roundKey[i]


def galoisMult(a, b):
    p = 0
    hiBitSet = 0
    for i in range(8):
        if b & 1 == 1:
            p ^= a
        hiBitSet = a & 0x80
        a <<= 1
        if hiBitSet == 0x80:
            a ^= 0x1b
        b >>= 1
    return p % 256


def mixColumn(column):
    temp = copy(column)
    column[0] = galoisMult(temp[0],2) ^ galoisMult(temp[3],1) ^ galoisMult(temp[2],1) ^ galoisMult(temp[1],3)
    column[1] = galoisMult(temp[1],2) ^ galoisMult(temp[0],1) ^ galoisMult(temp[3],1) ^ galoisMult(temp[2],3)
    column[2] = galoisMult(temp[2],2) ^ galoisMult(temp[1],1) ^ galoisMult(temp[0],1) ^ galoisMult(temp[3],3)
    column[3] = galoisMult(temp[3],2) ^ galoisMult(temp[2],1) ^ galoisMult(temp[1],1) ^ galoisMult(temp[0],3)


def mixColumnInv(column):
    temp = copy(column)
    column[0] = galoisMult(temp[0],14) ^ galoisMult(temp[3],9) ^ galoisMult(temp[2],13) ^ galoisMult(temp[1],11)
    column[1] = galoisMult(temp[1],14) ^ galoisMult(temp[0],9) ^ galoisMult(temp[3],13) ^ galoisMult(temp[2],11)
    column[2] = galoisMult(temp[2],14) ^ galoisMult(temp[1],9) ^ galoisMult(temp[0],13) ^ galoisMult(temp[3],11)
    column[3] = galoisMult(temp[3],14) ^ galoisMult(temp[2],9) ^ galoisMult(temp[1],13) ^ galoisMult(temp[0],11)


def mixColumns(state):
    for i in range(4):
        column = []
        for j in range(4):
            column.append(state[j*4+i])
        mixColumn(column)
        for j in range(4):
            state[j*4+i] = column[j]


def mixColumnsInv(state):
    for i in range(4):
        column = []
        for j in range(4):
            column.append(state[j*4+i])
        mixColumnInv(column)
        for j in range(4):
            state[j*4+i] = column[j]


def aesRound(state, roundKey):
    subBytes(state)
    shiftRows(state)
    mixColumns(state)
    addRoundKey(state, roundKey)


def aesRoundInv(state, roundKey):
    addRoundKey(state, roundKey)
    mixColumnsInv(state)
    shiftRowsInv(state)
    subBytesInv(state)



def createRoundKey(expandedKey, n):
    return expandedKey[(n*16):(n*16+16)]


def passwordToKey(password,mode=TYPES.B256):
    if mode == TYPES.B128:
        sha = hashlib.md5()
    elif mode == TYPES.B256 :
        sha = hashlib.sha256()
    else:
        raise Exception("Can not found aes mode")
    sha.update(password.encode("utf-8"))
    key = []
    for c in list(sha.digest()):
        key.append(ord(c))
    return key

def aesMain(state, expandedKey, numRounds=14):
    roundKey = createRoundKey(expandedKey, 0)
    addRoundKey(state, roundKey)
    for i in range(1, numRounds):
        roundKey = createRoundKey(expandedKey, i)
        aesRound(state, roundKey)
    # final round - leave out the mixColumns transformation
    roundKey = createRoundKey(expandedKey, numRounds)
    subBytes(state)
    shiftRows(state)
    addRoundKey(state, roundKey)

def aesMainInv(state, expandedKey, numRounds=14):
    roundKey = createRoundKey(expandedKey, numRounds)
    addRoundKey(state, roundKey)
    shiftRowsInv(state)
    subBytesInv(state)
    for i in range(numRounds-1,0,-1):
        roundKey = createRoundKey(expandedKey, i)
        aesRoundInv(state, roundKey)
    roundKey = createRoundKey(expandedKey, 0)
    addRoundKey(state, roundKey)
    

def aesEncrypt(plaintext, key , mode = TYPES.B256):
        block = copy(plaintext)
        rowNumber = 10
        if mode == TYPES.B256 :
                rowNumber = 14
        expandedKey = expandKey(key,mode)
        aesMain(block, expandedKey,rowNumber)
        return block

def aesDecrypt(plaintext, key , mode = TYPES.B256):
        block = copy(plaintext)
        rowNumber = 10
        if mode == TYPES.B256 :
                rowNumber = 14
        expandedKey = expandKey(key,mode)
        aesMainInv(block, expandedKey,rowNumber)
        return block





def getBlockStr(str,number=0):
    if number == 0 or len(str) >= ((number * 16) + 16):
        raw = str[(number * 16): ((number * 16) + 16)]
    else:
        return ""
    if len(raw) == 0:
        return ""
    block = []
    for c in list(raw):
        block.append(ord(c))
    if len(block) < 16:
        while len(block) < 16:
            block.append(0)
    return block

def blockToText(block):
    return  ''.join(map(unichr,block))


def textToBlock(text):
    return [ord(c) for c in text]

CODES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
def base64Encode(block):
    i = 0
    str = ""
    while i<len(block):
        b = (block[i] & 0xFC ) >> 2
        str = str + CODES[b]
        b = (block[i] & 0x03) << 4
        if (i + 1) < len(block):
            b = ((block[i + 1] & 0xF0) >> 4 ) | b
            str = str + CODES[b]
            b = (block[i + 1] & 0x0F) << 2
            if (i + 2 ) < len(block):
                b = ((block[i + 2] & 0xC0) >> 6) | b
                str = str + CODES[b]
                b = block[i + 2] & 0x3F
                str = str + CODES[b]
            else:
                str = str + CODES[b]
                str = str + "=="
        else:
            str = str + CODES[b]
            str = str + "=="
        i = i + 3
    return str

def EKKModeEncrypt(password,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    input = base64.b64encode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    while block != "":
        blockKey = aesEncrypt(block, aesKey,mode)
        ciphertext = ciphertext + blockKey
        number = number + 1
        block = getBlockStr(input, number)
    return base64Encode(ciphertext)


def EKKModeDecrypt(password,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    input = base64.b64decode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    while block != "":
        blockKey = aesDecrypt(block, aesKey,mode)
        ciphertext = ciphertext + blockKey
        number = number + 1
        block = getBlockStr(input, number)
    return base64.b64decode(blockToText(ciphertext))

def SBZModeEncrypt(password,bv,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    bv = base64.b64encode(bv)
    input = base64.b64encode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    BV = passwordToKey(bv)
    tmpBlock = []
    while block != "":
        tmpBlock = [block[i] ^ BV[i] for i in range(len(block))]
        blockKey = aesEncrypt(tmpBlock, aesKey,mode)
        ciphertext = ciphertext + blockKey
        BV = blockKey
        number = number + 1
        block = getBlockStr(input, number)
    return base64Encode(ciphertext)

def SBZModeDecrypt(password,bv,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    bv = base64.b64encode(bv)
    input = base64.b64decode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    BV = passwordToKey(bv)
    tmpBlock = []
    while block != "":
        blockKey = aesDecrypt(block, aesKey,mode)
        tmpBlock = [blockKey[i] ^ BV[i] for i in range(len(blockKey))]
        ciphertext = ciphertext + tmpBlock
        BV = block
        number = number + 1
        block = getBlockStr(input, number)
    return base64.b64decode(blockToText(ciphertext))

def CGBModeEncrypt(password,bv,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    bv = base64.b64encode(bv)
    input = base64.b64encode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    BV = passwordToKey(bv,TYPES.B128)
    tmpBlock = []
    while block != "":
        blockKey = aesEncrypt(BV, aesKey, mode)
        tmpBlock = [block[i] ^ blockKey[i] for i in range(len(blockKey))]
        ciphertext = ciphertext + tmpBlock
        BV = blockKey
        number = number + 1
        block = getBlockStr(input, number)
    return  base64Encode(ciphertext)

def CGBModeDecrypt(password,bv,input,mode = TYPES.B256):
    if not password or not password.strip():
        raise Exception("Password can not be empty")
    if not input and not input.strip():
        raise Exception("Input can not be empty")
    ciphertext = []
    number = 0

    password = base64.b64encode(password)
    bv = base64.b64encode(bv)
    input = base64.b64decode(input)

    aesKey = passwordToKey(password, mode)
    block = getBlockStr(input, number)
    BV = passwordToKey(bv,TYPES.B128)
    tmpBlock = []
    while block != "":
        blockKey = aesEncrypt(BV, aesKey, mode)
        tmpBlock = [block[i] ^ blockKey[i] for i in range(len(blockKey))]
        ciphertext = ciphertext + tmpBlock
        BV = blockKey
        number = number + 1
        block = getBlockStr(input, number)
    return base64.b64decode(blockToText(ciphertext))

def affine(str):
    bit = int(str[0],2)
    for i in xrange(1,len(str)):
        bit = bit ^ int(str[i],2)
    return bit

def affineBit(s):
    return str(s) if s <= 1 else bin(s >> 1) + str(s & 1)

def LatElementCal(r,c,si,so,number):
    count = 0
    for i in xrange(len(si)):
        if affine(affineBit(int(r & si[i]))) == affine(affineBit(int(c & so[i]))):
            count = count + 1
    return  count - pow(2,(number-1))

def LatTable(si,so,row,column):
    table = []
    for i in xrange(0,pow(2,row)):
        tempColumn = []
        for j in xrange(0,pow(2,column)):
            tempColumn.append(LatElementCal(i,j,si,so,row))
        table.append(tempColumn)
    return table


def DifferentialTableCalculate(a,b,si,so):
    count = 0
    for i in xrange(len(so)):
        index = int(a ^ si[i])
        if  (so[i] ^ so[index]) == b:
            count = count + 1
    return count



def  DifferentialTable(si,so,row,column):
    table = []
    for i in xrange(0,pow(2,row)):
        tempColumn = []
        for j in xrange(0,pow(2,column)):
            tempColumn.append(DifferentialTableCalculate(i,j,si,so))
        table.append(tempColumn)
    return table

def Euclid(num1, num2, division=None,maxNumbers=None,minNumbers=None):
    if num1 <= 0 or num2 <= 0:
        raise Exception('These numbers can not be at least one')
    BigNumber = max(num1, num2)
    SmallNumber = min(num1, num2)
    if division == None:
        division = list()
        maxNumbers = list()
        minNumbers = list()
    division.append(int(BigNumber / SmallNumber))
    maxNumbers.append(BigNumber)
    minNumbers.append(SmallNumber)
    if (BigNumber % SmallNumber) == 0:
        return SmallNumber, division, maxNumbers, minNumbers
    else:
        return Euclid((int)(BigNumber % SmallNumber), SmallNumber, division,maxNumbers,minNumbers)

def Phi(number):
    result = float(number)
    i=2
    while (i*i) <= number:
        if (number % i) == 0:
           while ((number % i) == 0):
               number /= i
           result -= (result/i)
        i += 1
    if number > 1:
        result -= (result/number)
    return int(result)

def CalculateInverseByCoprimeNumber(division):
    x = [0,1]
    y = [1,0]
    if len(division) > 2:
        for i in range(2,len(division)+2):
            x.append((division[i-2]*x[i-1])+x[i-2])
            y.append((division[i-2]*y[i-1])+y[i-2])
    if x[len(x)-1]/2 < y[len(y)-1]:
        return x[len(x)-1] - x[len(x)-2]
    elif x[len(x)-2] == 0:
        return division[len(division)-1]
    else:
        ret